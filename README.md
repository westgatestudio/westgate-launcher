## Welcome Westgate Studio's Repo

![image](https://bytebucket.org/westgatestudio/westgate-launcher/raw/f517f6e9760d7eee45f0147b60e8e320eba80438/src/assets/img/Westgate_Logo.Transparent-365x63.png)

---

#### Note on Third-Party Usage

You may use this software in your own project so long as the following conditions are met. Else,
is better to just run the launcher at your own risk. However, contribution as open-source are most
welcomed in our back-yard!

* Credit is expressly given to the original authors (Michael Moore).
  * Include a link to the original source on the launcher's About page.
  * Credit the authors and provide a link to the original source in any publications or download pages.
* The source code remain **public** as a fork of this repository.

We reserve the right to update these conditions at any time, please check back periodically.



## Development


### Getting Started



**System Requirements**

* [Node.js][nodejs] v10.x.x

---

Build for a specific platform.

| Platform    | Command              |
| ----------- | -------------------- |
| Windows x64 | `npm run dist:win`   |
| macOS       | `npm run dist:mac`   |
| Linux x64   | `npm run dist:linux` |

Builds for macOS may not work on Windows/Linux and vice-versa.

---

## Resources
The best way to contact the developers is on Discord.

 [![Discord](https://discordapp.com/api/guilds/231623609637011456/embed.png?style=banner2&theme=dark)](https://discord.io/WestGate)

### What We Use for Developments

- Node JS: (https://nodejs.org/en/)
- Visual Studio Code: (https://code.visualstudio.com/)
- WebStorm: (https://www.jetbrains.com/webstorm/features/)
- Main Process: (https://electronjs.org/docs/tutorial/application-architecture#main-and-renderer-processes) 
- Renderer Process: (https://electronjs.org/docs/tutorial/application-architecture#main-and-renderer-processes) 
- Debugger for Chrome: (https://marketplace.visualstudio.com/items?itemName=msjsdiag.debugger-for-chrome) 
- Discord: (https://discord.io/WestGate) 
- Messenger: (https://m.me/westgatestudio)
- Wiki: https://bitbucket.org/westgatestudio/westgate-launcher/wiki/Home)
- Atlassian: (https://developer.atlassian.com) 


### Westgate Studio's Main Colors

|    Colors     |
| -----------   |
| `#E1D9CE  `   |
| `#7289DA  `   |
| `#99AAB5  `   |
| `#A6C538  `   |

## Westgate Minecraft Servers

| Server      | Server IP              | Devices  |
| ----------- | ---------------------- |----------
| Westgate I  |`104.154.102.205:19112` | XBOX     |
| Westgate I  |`104.154.102.205:19112` | Win 10   |  
| Westgate I  |`104.154.102.205:19112` | PE       |  
| Westgate II |`BB58076.pocket.pe:58076`| XBOX    |
| Westgate II |`BB58076.pocket.pe:58076`| Win 10  |  
| Westgate II |`BB58076.pocket.pe:58076`| PE      |  
| Westgate III|`studiotwentynine.play.net.co`|Java(PC) |  
| Westgate IV |`192.3.191.147:25633`   |Java(PC) |  



| Notes  |
| ----------- |
|`*Server names will be updated soon without notice`|
|`*It is better to check back periodically for updates until the launcher is ready for production`|
|`*PE stands for Pocket Edition, which caters to Android Phones /Android Tablets/ Chromebook` |
